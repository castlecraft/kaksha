import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ModalController } from '@ionic/angular';

import { ImportantNotePage } from './important-note.page';
import { CourseService } from '../course/course.service';
import { of, BehaviorSubject } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ImportantNotesService } from './important-notes.service';

describe('ImportantNotePage', () => {
  let component: ImportantNotePage;
  let fixture: ComponentFixture<ImportantNotePage>;
  const selectedCourse = new BehaviorSubject<string>('');

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ImportantNotePage],
      imports: [HttpClientTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: CourseService,
          useValue: {
            getCourse: (...args) => of([{ topics: [] }]),
            selectedCourse,
            getCourseList: (...args) => of([{}]),
          },
        },
        {
          provide: ImportantNotesService,
          useValue: {
            getNote: (...args) => of([{}]),
          },
        },
        {
          provide: ModalController,
          useValue: {},
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ImportantNotePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
