import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { IonicModule } from '@ionic/angular';

import { ImportantNotePageRoutingModule } from './important-note-routing.module';

import { ImportantNotePage } from './important-note.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaterialModule,
    ImportantNotePageRoutingModule,
  ],
  declarations: [ImportantNotePage],
})
export class ImportantNotePageModule {}
