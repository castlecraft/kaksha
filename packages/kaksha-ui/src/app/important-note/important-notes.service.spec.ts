import { TestBed } from '@angular/core/testing';

import { ImportantNotesService } from './important-notes.service';
import { StorageService } from '../common/services/storage/storage.service';

import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ImportantNotesService', () => {
  let service: ImportantNotesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: StorageService,
          useValue: {
            getItem: (...args) => Promise.resolve('ITEM'),
          },
        },
      ],
    });
    service = TestBed.inject(ImportantNotesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
