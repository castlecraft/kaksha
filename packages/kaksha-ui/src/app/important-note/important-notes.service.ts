import { Injectable } from '@angular/core';

import { from } from 'rxjs';
import {
  ACCESS_TOKEN,
  AUTHORIZATION,
  BEARER_TOKEN_PREFIX,
} from '../constants/storage';
import { switchMap } from 'rxjs/operators';
import { ARTICLElIST_ENDPOINT } from '../constants/url-strings';
import { HttpClient, HttpParams } from '@angular/common/http';
import { StorageService } from '../common/services/storage/storage.service';
import { APIResponse } from '../common/interfaces/note.interface';

@Injectable({
  providedIn: 'root',
})
export class ImportantNotesService {
  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
  ) {}

  getNote(
    program: string,
    course_name: string,
    topic_name: string,
    search: string,
  ) {
    const params = new HttpParams()
      .set('program_name', program)
      .set('course_name', course_name)
      .set('topic_name', topic_name)
      .set('search', search);

    return from(this.storage.getItem(ACCESS_TOKEN)).pipe(
      switchMap((token) => {
        const headers = {
          [AUTHORIZATION]: BEARER_TOKEN_PREFIX + token,
        };
        return this.http.get<APIResponse>(ARTICLElIST_ENDPOINT, {
          params,
          headers,
        });
      }),
    );
  }

  addDraft(newDrafts, program, course_name) {}
}
