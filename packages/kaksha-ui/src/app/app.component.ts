import { Component } from '@angular/core';
import { AppService } from './app.service';
import { isDevMode } from '@angular/core';
import { OAuthService, AuthConfig, OAuthEvent } from 'angular-oauth2-oidc';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';
import {
  CLIENT_ID,
  REDIRECT_URI,
  SILENT_REFRESH_REDIRECT_URI,
  LOGIN_URL,
  ISSUER_URL,
  APP_URL,
} from './constants/storage';
import { SCOPE } from './constants/common';
import { Subscription } from 'rxjs';
import { StorageService } from './common/services/storage/storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  loggedIn: boolean = false;
  hideAuthButtons: boolean = false;
  subscription: Subscription;
  email: string;
  fullName: string;
  imageURL: string;
  constructor(
    private appService: AppService,
    private oauthService: OAuthService,
    private storageService: StorageService,
    private readonly router: Router,
  ) {
    this.setupOIDC();
  }

  ngOnInit() {
    this.oauthService.events.subscribe({
      next: ({ type }: OAuthEvent) => {
        switch (type) {
          case 'token_received':
            this.loggedIn = true;
            this.appService.getUserProfile().subscribe({
              next: (res: any) => {
                this.imageURL = res.picture;
                this.fullName = res.name;
              },
            });
            this.router.navigate(['home']);
            break;
        }
      },
      error: (error) => {},
    });
    this.loggedIn = this.oauthService.hasValidAccessToken();
    this.appService.getUserProfile().subscribe({
      next: (res: any) => {
        this.imageURL = res.picture;
        this.fullName = res.name;
      },
    });
  }

  setupOIDC(): void {
    this.appService.getMessage().subscribe((response) => {
      if (response.message) return; // { message: PLEASE_RUN_SETUP }
      this.appService.setInfoLocalStorage(response);
      const authConfig: AuthConfig = {
        clientId: localStorage.getItem(CLIENT_ID),
        redirectUri: localStorage.getItem(REDIRECT_URI),
        silentRefreshRedirectUri: localStorage.getItem(
          SILENT_REFRESH_REDIRECT_URI,
        ),
        loginUrl: localStorage.getItem(LOGIN_URL),
        scope: SCOPE,
        issuer: localStorage.getItem(ISSUER_URL),
      };

      if (isDevMode()) {
        authConfig.requireHttps = false;
        authConfig.disableAtHashCheck = true;
      }

      this.oauthService.configure(authConfig);
      this.oauthService.tokenValidationHandler = new JwksValidationHandler();
      this.oauthService.setupAutomaticSilentRefresh();
      this.oauthService.loadDiscoveryDocumentAndLogin();
    });
  }

  login() {
    this.oauthService.initImplicitFlow();
  }

  logout() {
    const logOutUrl =
      this.storageService.getInfo(ISSUER_URL) +
      '/auth/logout?redirect=' +
      this.storageService.getInfo(APP_URL);
    this.storageService.clearInfoLocalStorage();
    this.oauthService.logOut();
    this.loggedIn = false;
    window.location.href = logOutUrl;
  }
}
