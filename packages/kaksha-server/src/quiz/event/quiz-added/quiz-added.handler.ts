import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { QuizAddedEvent } from './quiz-added.event';
import { QuizService } from '../../entity/quiz/quiz.service';

@EventsHandler(QuizAddedEvent)
export class QuizAddedCommandHandler implements IEventHandler<QuizAddedEvent> {
  constructor(private readonly quizService: QuizService) {}
  async handle(event: QuizAddedEvent) {
    const { quiz } = event;
    await this.quizService.create(quiz);
  }
}
