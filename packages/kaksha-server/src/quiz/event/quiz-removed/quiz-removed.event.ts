import { IEvent } from '@nestjs/cqrs';
import { Quiz } from '../../entity/quiz/quiz.entity';

export class QuizRemovedEvent implements IEvent {
  constructor(public quiz: Quiz) {}
}
