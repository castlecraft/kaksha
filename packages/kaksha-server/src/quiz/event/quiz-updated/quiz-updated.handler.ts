import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { QuizUpdatedEvent } from './quiz-updated.event';
import { QuizService } from '../../entity/quiz/quiz.service';

@EventsHandler(QuizUpdatedEvent)
export class QuizUpdatedCommandHandler
  implements IEventHandler<QuizUpdatedEvent> {
  constructor(private readonly object: QuizService) {}

  async handle(event: QuizUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}
