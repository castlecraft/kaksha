import { InjectRepository } from '@nestjs/typeorm';
import { Quiz } from './quiz.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';

@Injectable()
export class QuizService {
  constructor(
    @InjectRepository(Quiz)
    private readonly quizRepository: MongoRepository<Quiz>,
  ) {}

  async find(query?) {
    return await this.quizRepository.find(query);
  }

  async create(quiz: Quiz) {
    const quizObject = new Quiz();
    Object.assign(quizObject, quiz);
    return await this.quizRepository.insertOne(quizObject);
  }

  async findOne(param, options?) {
    return await this.quizRepository.findOne(param, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.quizRepository.manager.connection
      .getMetadata(Quiz)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.quizRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.quizRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?) {
    return await this.quizRepository.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.quizRepository.updateOne(query, options);
  }
}
