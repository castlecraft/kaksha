import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddQuizCommand } from './add-quiz.command';
import { QuizAggregateService } from '../../aggregates/quiz-aggregate/quiz-aggregate.service';

@CommandHandler(AddQuizCommand)
export class AddQuizCommandHandler implements ICommandHandler<AddQuizCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: QuizAggregateService,
  ) {}
  async execute(command: AddQuizCommand) {
    const { quizPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addQuiz(quizPayload, clientHttpRequest);
    aggregate.commit();
  }
}
