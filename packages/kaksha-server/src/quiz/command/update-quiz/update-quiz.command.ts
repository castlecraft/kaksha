import { ICommand } from '@nestjs/cqrs';
import { UpdateQuizDto } from '../../entity/quiz/update-quiz-dto';

export class UpdateQuizCommand implements ICommand {
  constructor(public readonly updatePayload: UpdateQuizDto) {}
}
