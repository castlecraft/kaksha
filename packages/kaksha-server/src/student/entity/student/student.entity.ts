import { Column, ObjectIdColumn, BaseEntity, ObjectID, Entity } from 'typeorm';
export class Guardians {
  guardian: string;
  guardian_name: string;
  relation: string;
}

@Entity()
export class Student extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  docstatus: number;

  @Column()
  first_name: string;

  @Column()
  middle_name: string;

  @Column()
  last_name: string;

  @Column()
  student_email_id: string;

  @Column()
  student_mobile_number: string;

  @Column()
  joining_date: string;

  @Column()
  date_of_birth: string;

  @Column()
  gender: string;

  @Column()
  nationality: string;

  @Column()
  address_line_1: string;

  @Column()
  title: string;

  @Column()
  guardians: Guardians[];

  @Column()
  isSynced: boolean;
}
