import { RetrieveStudentQueryHandler } from './get-student/retrieve-student.handler';
import { RetrieveStudentListQueryHandler } from './list-student/retrieve-student-list.handler';

export const StudentQueryManager = [
  RetrieveStudentQueryHandler,
  RetrieveStudentListQueryHandler,
];
