import { IEvent } from '@nestjs/cqrs';
import { Program } from '../../entity/program/program.entity';

export class ProgramRemovedEvent implements IEvent {
  constructor(public program: Program) {}
}
