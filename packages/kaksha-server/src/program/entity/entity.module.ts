import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Program } from './program/program.entity';
import { ProgramService } from './program/program.service';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [TypeOrmModule.forFeature([Program]), CqrsModule],
  providers: [ProgramService],
  exports: [ProgramService],
})
export class ProgramEntitiesModule {}
