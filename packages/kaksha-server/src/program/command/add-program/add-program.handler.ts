import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddProgramCommand } from './add-program.command';
import { ProgramAggregateService } from '../../aggregates/program-aggregate/program-aggregate.service';

@CommandHandler(AddProgramCommand)
export class AddProgramCommandHandler
  implements ICommandHandler<AddProgramCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: ProgramAggregateService,
  ) {}
  async execute(command: AddProgramCommand) {
    const { programPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addProgram(programPayload, clientHttpRequest);
    aggregate.commit();
  }
}
