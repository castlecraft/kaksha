import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveProgramCommand } from './remove-program.command';
import { ProgramAggregateService } from '../../aggregates/program-aggregate/program-aggregate.service';

@CommandHandler(RemoveProgramCommand)
export class RemoveProgramCommandHandler
  implements ICommandHandler<RemoveProgramCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: ProgramAggregateService,
  ) {}
  async execute(command: RemoveProgramCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.remove(uuid);
    aggregate.commit();
  }
}
