import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveProgramListQuery } from './retrieve-program-list.query';
import { ProgramAggregateService } from '../../aggregates/program-aggregate/program-aggregate.service';

@QueryHandler(RetrieveProgramListQuery)
export class RetrieveProgramListQueryHandler
  implements IQueryHandler<RetrieveProgramListQuery> {
  constructor(private readonly manager: ProgramAggregateService) {}
  async execute(query: RetrieveProgramListQuery) {
    const { offset, limit, search, sort, clientHttpRequest } = query;
    return await this.manager.getProgramList(
      Number(offset),
      Number(limit),
      search,
      sort,
      clientHttpRequest,
    );
  }
}
