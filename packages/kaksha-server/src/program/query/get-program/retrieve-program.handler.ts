import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveProgramQuery } from './retrieve-program.query';
import { ProgramAggregateService } from '../../aggregates/program-aggregate/program-aggregate.service';

@QueryHandler(RetrieveProgramQuery)
export class RetrieveProgramQueryHandler
  implements IQueryHandler<RetrieveProgramQuery> {
  constructor(private readonly manager: ProgramAggregateService) {}

  async execute(query: RetrieveProgramQuery) {
    const { req, uuid } = query;
    return this.manager.retrieveProgram(uuid, req);
  }
}
