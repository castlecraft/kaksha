import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateSingleArticleCommand } from './update-single-article.command';
import { ArticleAggregateService } from '../../aggregates/article-aggregate/article-aggregate.service';

@CommandHandler(UpdateSingleArticleCommand)
export class UpdateSingleArticleCommandHandler
  implements ICommandHandler<UpdateSingleArticleCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: ArticleAggregateService,
  ) {}

  async execute(command: UpdateSingleArticleCommand) {
    const { updatePayload, req } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.updateSingleArticle(updatePayload, req).toPromise();
    aggregate.commit();
  }
}
