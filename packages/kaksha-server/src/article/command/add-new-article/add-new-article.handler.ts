import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { ArticleAggregateService } from '../../aggregates/article-aggregate/article-aggregate.service';
import { AddNewArticleCommand } from './add-new-article.command';

@CommandHandler(AddNewArticleCommand)
export class AddNewArticleCommandHandler
  implements ICommandHandler<AddNewArticleCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: ArticleAggregateService,
  ) {}
  async execute(command: AddNewArticleCommand) {
    const { addArticlePayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate
      .getExistingArticle(addArticlePayload, clientHttpRequest)
      .toPromise();
    aggregate.commit();
  }
}
