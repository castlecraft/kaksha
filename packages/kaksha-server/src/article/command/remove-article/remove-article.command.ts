import { ICommand } from '@nestjs/cqrs';
import { UpdateArticleDto } from '../../entity/article/update-article-dto';

export class RemoveArticleCommand implements ICommand {
  constructor(
    public readonly removeArticlePayload: UpdateArticleDto,
    public readonly req,
  ) {}
}
