import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveArticleListQuery } from './retrieve-article-list.query';
import { ArticleAggregateService } from '../../aggregates/article-aggregate/article-aggregate.service';

@QueryHandler(RetrieveArticleListQuery)
export class RetrieveArticleListQueryHandler
  implements IQueryHandler<RetrieveArticleListQuery> {
  constructor(private readonly manager: ArticleAggregateService) {}
  async execute(query: RetrieveArticleListQuery) {
    const {
      offset,
      limit,
      search,
      sort,
      clientHttpRequest,
      getArticleList,
    } = query;
    return await this.manager
      .getArticleList(
        Number(offset),
        Number(limit),
        search,
        sort,
        clientHttpRequest,
        getArticleList,
      )
      .toPromise();
  }
}
