import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveArticleQuery } from './retrieve-article.query';
import { ArticleAggregateService } from '../../aggregates/article-aggregate/article-aggregate.service';

@QueryHandler(RetrieveArticleQuery)
export class RetrieveArticleQueryHandler
  implements IQueryHandler<RetrieveArticleQuery> {
  constructor(private readonly manager: ArticleAggregateService) {}

  async execute(query: RetrieveArticleQuery) {
    const { req, uuid } = query;
    return this.manager.retrieveArticle(uuid, req);
  }
}
