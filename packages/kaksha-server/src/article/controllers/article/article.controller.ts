import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { ArticleDto } from '../../entity/article/article-dto';
import { AddArticleCommand } from '../../command/add-article/add-article.command';
import { RemoveArticleCommand } from '../../command/remove-article/remove-article.command';
import { UpdateArticleCommand } from '../../command/update-article/update-article.command';
import { RetrieveArticleQuery } from '../../query/get-article/retrieve-article.query';
import { RetrieveArticleListQuery } from '../../query/list-article/retrieve-article-list.query';
import { UpdateArticleDto } from '../../entity/article/update-article-dto';
import { AddNewArticleCommand } from '../../command/add-new-article/add-new-article.command';
import { UpdateSingleArticleCommand } from '../../command/update-single-article/update-single-article.command';
import { UpdateTopicStatusDto } from '../../../topic/entity/topic/update-topic-status-dto';

@Controller('article')
export class ArticleController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  createArticle(@Body() articlePayload: ArticleDto, @Req() req) {
    return this.commandBus.execute(new AddArticleCommand(articlePayload, req));
  }

  @Post('v1/remove')
  @UseGuards(TokenGuard)
  removeArticle(@Body() removeArticlePayload: UpdateArticleDto, @Req() req) {
    return this.commandBus.execute(
      new RemoveArticleCommand(removeArticlePayload, req),
    );
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getArticle(@Param('uuid') uuid: string, @Req() req) {
    return await this.queryBus.execute(new RetrieveArticleQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getArticleList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Query() getArticleList: UpdateTopicStatusDto,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveArticleListQuery(
        offset,
        limit,
        sort,
        search,
        clientHttpRequest,
        getArticleList,
      ),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateArticle(@Body() updatePayload: UpdateArticleDto) {
    return this.commandBus.execute(new UpdateArticleCommand(updatePayload));
  }

  @Post('v1/add_article')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  addArticle(@Body() addArticlePayload: UpdateArticleDto, @Req() req) {
    return this.commandBus.execute(
      new AddNewArticleCommand(addArticlePayload, req),
    );
  }

  @Post('v1/update_article')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateSingleArticle(@Body() updatePayload: UpdateArticleDto, @Req() req) {
    return this.commandBus.execute(
      new UpdateSingleArticleCommand(updatePayload, req),
    );
  }
}
