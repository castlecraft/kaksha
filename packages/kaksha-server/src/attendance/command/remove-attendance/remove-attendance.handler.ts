import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveAttendanceCommand } from './remove-attendance.command';
import { AttendanceAggregateService } from '../../aggregates/attendance-aggregate/attendance-aggregate.service';

@CommandHandler(RemoveAttendanceCommand)
export class RemoveAttendanceCommandHandler
  implements ICommandHandler<RemoveAttendanceCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: AttendanceAggregateService,
  ) {}
  async execute(command: RemoveAttendanceCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.remove(uuid);
    aggregate.commit();
  }
}
