import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { v4 as uuidv4 } from 'uuid';
import { AttendanceDto } from '../../entity/attendance/attendance-dto';
import { Attendance } from '../../entity/attendance/attendance.entity';
import { AttendanceAddedEvent } from '../../event/attendance-added/attendance-added.event';
import { AttendanceService } from '../../entity/attendance/attendance.service';
import { AttendanceRemovedEvent } from '../../event/attendance-removed/attendance-removed.event';
import { AttendanceUpdatedEvent } from '../../event/attendance-updated/attendance-updated.event';
import { UpdateAttendanceDto } from '../../entity/attendance/update-attendance-dto';
import { CourseScheduleService } from '../../../course-schedule/entity/course-schedule/course-schedule.service';
import { switchMap } from 'rxjs/operators';
import { throwError, of } from 'rxjs';

@Injectable()
export class AttendanceAggregateService extends AggregateRoot {
  constructor(
    private readonly attendanceService: AttendanceService,
    private readonly courseScheduleService: CourseScheduleService,
  ) {
    super();
  }

  addAttendance(attendancePayload: AttendanceDto, clientHttpRequest) {
    const attendance = new Attendance();
    Object.assign(attendance, attendancePayload);
    attendance.uuid = uuidv4();
    this.apply(new AttendanceAddedEvent(attendance, clientHttpRequest));
  }

  async retrieveAttendance(uuid: string, req) {
    const provider = await this.attendanceService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getAttendanceList(
    offset,
    limit,
    sort,
    search,
    course,
    date,
    clientHttpRequest,
  ) {
    return this.courseScheduleService
      .asyncAggregate([
        {
          $match: { course },
        },
        {
          $match: { schedule_date: date },
        },
        {
          $lookup: {
            from: 'attendance',
            localField: 'name',
            foreignField: 'course_schedule',
            as: 'student_attendance',
          },
        },
        {
          $project: {
            course: 1,
            schedule_date: 1,
            student_group: 1,
            'student_attendance.student_name': 1,
            'student_attendance.status': 1,
          },
        },
      ])
      .pipe(
        switchMap((student_attendance: any[]) => {
          if (!student_attendance || !student_attendance.length) {
            return throwError(new NotFoundException());
          }
          return of(student_attendance);
        }),
      );
  }

  async remove(uuid: string) {
    const found = await this.attendanceService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new AttendanceRemovedEvent(found));
  }

  async update(updatePayload: UpdateAttendanceDto) {
    const provider = await this.attendanceService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new AttendanceUpdatedEvent(update));
  }
}
