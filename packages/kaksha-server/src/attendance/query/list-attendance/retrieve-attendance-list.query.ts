import { IQuery } from '@nestjs/cqrs';

export class RetrieveAttendanceListQuery implements IQuery {
  constructor(
    public offset: number,
    public limit: number,
    public search: string,
    public sort: string,
    public course: string,
    public date: string,
    public clientHttpRequest: any,
  ) {}
}
