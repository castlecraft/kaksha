import { ICommand } from '@nestjs/cqrs';
import { UpdateQuestionDto } from '../../entity/question/update-question-dto';

export class RemoveQuestionCommand implements ICommand {
  constructor(
    public readonly removeQuestionPayload: UpdateQuestionDto,
    public readonly clientHttpRequest,
  ) {}
}
