import { AddQuestionCommandHandler } from './add-question/add-question.handler';
import { RemoveQuestionCommandHandler } from './remove-question/remove-question.handler';
import { UpdateQuestionCommandHandler } from './update-question/update-question.handler';

export const QuestionCommandManager = [
  AddQuestionCommandHandler,
  RemoveQuestionCommandHandler,
  UpdateQuestionCommandHandler,
];
