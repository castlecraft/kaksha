import {
  IsOptional,
  IsString,
  IsNumber,
  ValidateNested,
  IsNotEmpty,
} from 'class-validator';
import { Type } from 'class-transformer';

export class QuestionDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsNotEmpty()
  @IsString()
  question: string;

  @IsNotEmpty()
  @IsString()
  topic_name: string;

  @IsOptional()
  @IsString()
  question_type: string;

  @IsOptional()
  @IsString()
  doctype: string;

  @ValidateNested()
  @Type(() => OptionsDto)
  options: OptionsDto[];
}
export class OptionsDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsNotEmpty()
  @IsString()
  option: string;

  @IsNotEmpty()
  @IsNumber()
  is_correct: number;

  @IsOptional()
  @IsString()
  doctype: string;
}
