import { IEvent } from '@nestjs/cqrs';
import { Question } from '../../entity/question/question.entity';

export class QuestionUpdatedEvent implements IEvent {
  constructor(public updatePayload: Question) {}
}
