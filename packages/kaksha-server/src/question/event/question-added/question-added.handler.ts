import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { QuestionAddedEvent } from './question-added.event';
import { QuestionService } from '../../entity/question/question.service';

@EventsHandler(QuestionAddedEvent)
export class QuestionAddedCommandHandler
  implements IEventHandler<QuestionAddedEvent> {
  constructor(private readonly questionService: QuestionService) {}
  async handle(event: QuestionAddedEvent) {
    const { question } = event;
    await this.questionService.create(question);
  }
}
