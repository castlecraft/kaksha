import { IEvent } from '@nestjs/cqrs';
import { Question } from '../../entity/question/question.entity';

export class QuestionAddedEvent implements IEvent {
  constructor(public question: Question, public clientHttpRequest: any) {}
}
