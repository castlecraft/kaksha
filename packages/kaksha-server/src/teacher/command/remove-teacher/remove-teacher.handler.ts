import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveTeacherCommand } from './remove-teacher.command';
import { TeacherAggregateService } from '../../aggregates/teacher-aggregate/teacher-aggregate.service';

@CommandHandler(RemoveTeacherCommand)
export class RemoveTeacherCommandHandler
  implements ICommandHandler<RemoveTeacherCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: TeacherAggregateService,
  ) {}
  async execute(command: RemoveTeacherCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.remove(uuid);
    aggregate.commit();
  }
}
