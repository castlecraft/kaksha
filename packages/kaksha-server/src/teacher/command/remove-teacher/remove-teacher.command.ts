import { ICommand } from '@nestjs/cqrs';

export class RemoveTeacherCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
