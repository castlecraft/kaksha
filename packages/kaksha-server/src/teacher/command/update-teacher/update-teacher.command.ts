import { ICommand } from '@nestjs/cqrs';
import { UpdateTeacherDto } from '../../entity/teacher/update-teacher-dto';

export class UpdateTeacherCommand implements ICommand {
  constructor(public readonly updatePayload: UpdateTeacherDto) {}
}
