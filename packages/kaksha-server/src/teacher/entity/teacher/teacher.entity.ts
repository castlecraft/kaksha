import { Column, ObjectIdColumn, BaseEntity, ObjectID, Entity } from 'typeorm';

@Entity()
export class Teacher extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  docstatus: number;

  @Column()
  instructor_name: string;

  @Column()
  employee: string;

  @Column()
  doctype: string;

  @Column()
  instructor_log: TeacherLogDto[];

  @Column()
  instructor_email: string;

  @Column()
  isSynced: boolean;
}
export class TeacherLogDto {
  name: string;
  docstatus: number;
  academic_year: string;
  academic_term: string;
  program: string;
  course: string;
  doctype: string;
}
