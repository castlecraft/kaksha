import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveTeacherListQuery } from './retrieve-teacher-list.query';
import { TeacherAggregateService } from '../../aggregates/teacher-aggregate/teacher-aggregate.service';

@QueryHandler(RetrieveTeacherListQuery)
export class RetrieveTeacherListQueryHandler
  implements IQueryHandler<RetrieveTeacherListQuery> {
  constructor(private readonly manager: TeacherAggregateService) {}
  async execute(query: RetrieveTeacherListQuery) {
    const { offset, limit, search, sort, clientHttpRequest } = query;
    return await this.manager.getTeacherList(
      Number(offset),
      Number(limit),
      search,
      sort,
      clientHttpRequest,
    );
  }
}
