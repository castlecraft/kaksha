import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveTeacherQuery } from './retrieve-teacher.query';
import { TeacherAggregateService } from '../../aggregates/teacher-aggregate/teacher-aggregate.service';

@QueryHandler(RetrieveTeacherQuery)
export class RetrieveTeacherQueryHandler
  implements IQueryHandler<RetrieveTeacherQuery> {
  constructor(private readonly manager: TeacherAggregateService) {}

  async execute(query: RetrieveTeacherQuery) {
    const { req, uuid } = query;
    return this.manager.retrieveTeacher(uuid, req);
  }
}
