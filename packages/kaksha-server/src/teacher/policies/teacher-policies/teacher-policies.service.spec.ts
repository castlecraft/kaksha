import { Test, TestingModule } from '@nestjs/testing';
import { TeacherPoliciesService } from './teacher-policies.service';

describe('TeacherPoliciesService', () => {
  let service: TeacherPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TeacherPoliciesService],
    }).compile();

    service = module.get<TeacherPoliciesService>(TeacherPoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
