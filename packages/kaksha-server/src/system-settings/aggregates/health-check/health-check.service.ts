import {
  HealthIndicatorFunction,
  TypeOrmHealthIndicator,
} from '@nestjs/terminus';
import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { InjectConnection } from '@nestjs/typeorm';
import {
  DEFAULT,
  TOKEN_CACHE_CONNECTION,
} from '../../../constants/typeorm.connection';

@Injectable()
export class HealthCheckAggregateService {
  constructor(
    @InjectConnection(DEFAULT)
    private readonly defaultDbConnection: Connection,
    @InjectConnection(TOKEN_CACHE_CONNECTION)
    private readonly tokenCacheDbConnection: Connection,
    private readonly database: TypeOrmHealthIndicator,
  ) {}

  createTerminusOptions(): HealthIndicatorFunction[] {
    const healthEndpoint: HealthIndicatorFunction[] = [
      async () =>
        this.database.pingCheck('database', {
          connection: this.defaultDbConnection,
        }),
      async () =>
        this.database.pingCheck('cache', {
          connection: this.tokenCacheDbConnection,
        }),
    ];

    return healthEndpoint;
  }
}
