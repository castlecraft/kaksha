import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmHealthIndicator } from '@nestjs/terminus';
import { HealthCheckAggregateService } from './health-check.service';
import { getConnectionToken } from '@nestjs/typeorm';
import {
  DEFAULT,
  TOKEN_CACHE_CONNECTION,
} from '../../../constants/typeorm.connection';

describe('HealthCheckAggregateService', () => {
  let service: HealthCheckAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        HealthCheckAggregateService,
        { provide: getConnectionToken(DEFAULT), useValue: {} },
        { provide: getConnectionToken(TOKEN_CACHE_CONNECTION), useValue: {} },
        { provide: TypeOrmHealthIndicator, useValue: {} },
      ],
    }).compile();

    service = module.get<HealthCheckAggregateService>(
      HealthCheckAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
