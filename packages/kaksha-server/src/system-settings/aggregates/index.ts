import { HealthCheckAggregateService } from './health-check/health-check.service';
import { SettingsService } from './settings/settings.service';

export const SystemSettingsAggregates = [
  SettingsService,
  HealthCheckAggregateService,
];
