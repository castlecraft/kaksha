import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { CourseScheduleService } from './course-schedule.service';
import { CourseSchedule } from './course-schedule.entity';

describe('courseScheduleService', () => {
  let service: CourseScheduleService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CourseScheduleService,
        {
          provide: getRepositoryToken(CourseSchedule),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<CourseScheduleService>(CourseScheduleService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
