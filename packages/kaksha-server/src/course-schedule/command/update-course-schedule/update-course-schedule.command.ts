import { ICommand } from '@nestjs/cqrs';
import { UpdateCourseScheduleDto } from '../../entity/course-schedule/update-course-schedule-dto';

export class UpdateCourseScheduleCommand implements ICommand {
  constructor(public readonly updatePayload: UpdateCourseScheduleDto) {}
}
