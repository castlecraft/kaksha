import { Test, TestingModule } from '@nestjs/testing';
import { TopicPoliciesService } from './topic-policies.service';

describe('TopicPoliciesService', () => {
  let service: TopicPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TopicPoliciesService],
    }).compile();

    service = module.get<TopicPoliciesService>(TopicPoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
