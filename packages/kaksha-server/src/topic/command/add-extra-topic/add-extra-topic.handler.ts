import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddExtraTopicCommand } from './add-extra-topic.command';
import { TopicAggregateService } from '../../aggregates/topic-aggregate/topic-aggregate.service';

@CommandHandler(AddExtraTopicCommand)
export class AddExtraTopicCommandHandler
  implements ICommandHandler<AddExtraTopicCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: TopicAggregateService,
  ) {}
  async execute(command: AddExtraTopicCommand) {
    const { addTopicPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate
      .addExtraTopic(addTopicPayload, clientHttpRequest)
      .toPromise();
    aggregate.commit();
  }
}
