import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddTopicCommand } from './add-topic.command';
import { TopicAggregateService } from '../../aggregates/topic-aggregate/topic-aggregate.service';

@CommandHandler(AddTopicCommand)
export class AddTopicCommandHandler
  implements ICommandHandler<AddTopicCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: TopicAggregateService,
  ) {}
  async execute(command: AddTopicCommand) {
    const { topicPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addTopic(topicPayload, clientHttpRequest);
    aggregate.commit();
  }
}
