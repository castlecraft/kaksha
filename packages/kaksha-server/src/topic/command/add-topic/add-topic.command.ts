import { ICommand } from '@nestjs/cqrs';
import { TopicDto } from '../../entity/topic/topic-dto';

export class AddTopicCommand implements ICommand {
  constructor(
    public topicPayload: TopicDto,
    public readonly clientHttpRequest: any,
  ) {}
}
