import { ICommand } from '@nestjs/cqrs';

export class RemoveTopicCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
