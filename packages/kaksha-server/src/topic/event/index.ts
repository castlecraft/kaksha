import { TopicAddedEventHandler } from './topic-added/topic-added.handler';
import { TopicRemovedEventHandler } from './topic-removed/topic-removed.handler';
import { TopicUpdatedEventHandler } from './topic-updated/topic-updated.handler';
import { TopicStatusUpdatedEventHandler } from './topic-status-updated/topic-status-updated.handler';

export const TopicEventManager = [
  TopicAddedEventHandler,
  TopicRemovedEventHandler,
  TopicUpdatedEventHandler,
  TopicStatusUpdatedEventHandler,
];
