import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TopicService } from '../../entity/topic/topic.service';
import { TopicRemovedEvent } from './topic-removed.event';

@EventsHandler(TopicRemovedEvent)
export class TopicRemovedEventHandler
  implements IEventHandler<TopicRemovedEvent> {
  constructor(private readonly topicService: TopicService) {}
  async handle(event: TopicRemovedEvent) {
    const { topic } = event;
    await this.topicService.deleteOne({ uuid: topic.uuid });
  }
}
