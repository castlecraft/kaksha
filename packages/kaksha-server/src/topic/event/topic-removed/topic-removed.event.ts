import { IEvent } from '@nestjs/cqrs';
import { Topic } from '../../entity/topic/topic.entity';

export class TopicRemovedEvent implements IEvent {
  constructor(public topic: Topic) {}
}
