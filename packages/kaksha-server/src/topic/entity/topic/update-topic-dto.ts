import {
  IsNotEmpty,
  IsString,
  IsOptional,
  IsNumber,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
export class UpdateTopicDto {
  @IsNotEmpty()
  uuid: string;

  @IsString()
  @IsOptional()
  name: string;

  @IsNumber()
  @IsOptional()
  docstatus: number;

  @IsString()
  @IsOptional()
  topic_name: string;

  @IsString()
  @IsOptional()
  doctype: string;

  @ValidateNested()
  @Type(() => UpdateTopicContentDto)
  topic_content: UpdateTopicContentDto[];
}
export class UpdateTopicContentDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsNumber()
  @IsOptional()
  docstatus: number;

  @IsString()
  @IsOptional()
  content_type: string;

  @IsString()
  @IsOptional()
  content: string;

  @IsString()
  @IsOptional()
  doctype: string;
}
