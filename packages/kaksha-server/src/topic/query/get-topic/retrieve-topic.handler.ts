import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveTopicQuery } from './retrieve-topic.query';
import { TopicAggregateService } from '../../aggregates/topic-aggregate/topic-aggregate.service';

@QueryHandler(RetrieveTopicQuery)
export class RetrieveTopicQueryHandler
  implements IQueryHandler<RetrieveTopicQuery> {
  constructor(private readonly manager: TopicAggregateService) {}

  async execute(query: RetrieveTopicQuery) {
    const { req, uuid } = query;
    return this.manager.retrieveTopic(uuid, req);
  }
}
