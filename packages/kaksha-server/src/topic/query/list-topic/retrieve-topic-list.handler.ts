import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveTopicListQuery } from './retrieve-topic-list.query';
import { TopicAggregateService } from '../../aggregates/topic-aggregate/topic-aggregate.service';

@QueryHandler(RetrieveTopicListQuery)
export class RetrieveTopicListQueryHandler
  implements IQueryHandler<RetrieveTopicListQuery> {
  constructor(private readonly manager: TopicAggregateService) {}
  async execute(query: RetrieveTopicListQuery) {
    const { offset, limit, search, sort, clientHttpRequest } = query;
    return await this.manager.getTopicList(
      Number(offset),
      Number(limit),
      search,
      sort,
      clientHttpRequest,
    );
  }
}
