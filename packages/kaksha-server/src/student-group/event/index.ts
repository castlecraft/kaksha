import { StudentGroupAddedCommandHandler } from './student-group-added/student-group-added.handler';
import { StudentGroupRemovedCommandHandler } from './student-group-removed/student-group-removed.handler';
import { StudentGroupUpdatedCommandHandler } from './student-group-updated/student-group-updated.handler';

export const StudentGroupEventManager = [
  StudentGroupAddedCommandHandler,
  StudentGroupRemovedCommandHandler,
  StudentGroupUpdatedCommandHandler,
];
