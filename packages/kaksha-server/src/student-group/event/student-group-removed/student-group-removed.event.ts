import { IEvent } from '@nestjs/cqrs';
import { StudentGroup } from '../../entity/student-group/student-group.entity';

export class StudentGroupRemovedEvent implements IEvent {
  constructor(public studentGroup: StudentGroup) {}
}
