import { Test, TestingModule } from '@nestjs/testing';
import { StudentGroupAggregateService } from './student-group-aggregate.service';
import { StudentGroupService } from '../../entity/student-group/student-group.service';

describe('StudentGroupAggregateService', () => {
  let service: StudentGroupAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StudentGroupAggregateService,
        {
          provide: StudentGroupService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<StudentGroupAggregateService>(
      StudentGroupAggregateService,
    );
  });
  StudentGroupAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
