import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { StudentGroupService } from './student-group.service';
import { StudentGroup } from './student-group.entity';

describe('studentGroupService', () => {
  let service: StudentGroupService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StudentGroupService,
        {
          provide: getRepositoryToken(StudentGroup),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<StudentGroupService>(StudentGroupService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
