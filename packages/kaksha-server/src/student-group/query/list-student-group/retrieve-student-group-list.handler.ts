import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveStudentGroupListQuery } from './retrieve-student-group-list.query';
import { StudentGroupAggregateService } from '../../aggregates/student-group-aggregate/student-group-aggregate.service';

@QueryHandler(RetrieveStudentGroupListQuery)
export class RetrieveStudentGroupListQueryHandler
  implements IQueryHandler<RetrieveStudentGroupListQuery> {
  constructor(private readonly manager: StudentGroupAggregateService) {}
  async execute(query: RetrieveStudentGroupListQuery) {
    const { offset, limit, search, sort, clientHttpRequest } = query;
    return await this.manager.getStudentGroupList(
      Number(offset),
      Number(limit),
      search,
      sort,
      clientHttpRequest,
    );
  }
}
