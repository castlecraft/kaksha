import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveStudentGroupQuery } from './retrieve-student-group.query';
import { StudentGroupAggregateService } from '../../aggregates/student-group-aggregate/student-group-aggregate.service';

@QueryHandler(RetrieveStudentGroupQuery)
export class RetrieveStudentGroupQueryHandler
  implements IQueryHandler<RetrieveStudentGroupQuery> {
  constructor(private readonly manager: StudentGroupAggregateService) {}

  async execute(query: RetrieveStudentGroupQuery) {
    const { req, uuid } = query;
    return this.manager.retrieveStudentGroup(uuid, req);
  }
}
