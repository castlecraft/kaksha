import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddStudentGroupCommand } from './add-student-group.command';
import { StudentGroupAggregateService } from '../../aggregates/student-group-aggregate/student-group-aggregate.service';

@CommandHandler(AddStudentGroupCommand)
export class AddStudentGroupCommandHandler
  implements ICommandHandler<AddStudentGroupCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: StudentGroupAggregateService,
  ) {}
  async execute(command: AddStudentGroupCommand) {
    const { studentGroupPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addStudentGroup(studentGroupPayload, clientHttpRequest);
    aggregate.commit();
  }
}
