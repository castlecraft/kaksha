import { AddDepartmentCommandHandler } from './add-department/add-department.handler';
import { RemoveDepartmentCommandHandler } from './remove-department/remove-department.handler';
import { UpdateDepartmentCommandHandler } from './update-department/update-department.handler';

export const DepartmentCommandManager = [
  AddDepartmentCommandHandler,
  RemoveDepartmentCommandHandler,
  UpdateDepartmentCommandHandler,
];
