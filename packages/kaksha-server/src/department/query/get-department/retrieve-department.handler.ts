import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveDepartmentQuery } from './retrieve-department.query';
import { DepartmentAggregateService } from '../../aggregates/department-aggregate/department-aggregate.service';

@QueryHandler(RetrieveDepartmentQuery)
export class RetrieveDepartmentQueryHandler
  implements IQueryHandler<RetrieveDepartmentQuery> {
  constructor(private readonly manager: DepartmentAggregateService) {}

  async execute(query: RetrieveDepartmentQuery) {
    const { req, uuid } = query;
    return this.manager.retrieveDepartment(uuid, req);
  }
}
