import {
  IsNotEmpty,
  IsOptional,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
export class UpdateDepartmentDto {
  @IsNotEmpty()
  uuid: string;

  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  department_name: string;

  @IsOptional()
  @IsString()
  parent_department: string;

  @IsOptional()
  @IsString()
  company: string;

  @IsOptional()
  @IsNumber()
  is_group: number;

  @IsOptional()
  @IsNumber()
  disabled: number;

  @IsOptional()
  @IsNumber()
  lft: number;

  @IsOptional()
  @IsNumber()
  rgt: number;

  @IsOptional()
  @IsString()
  old_parent: string;

  @IsOptional()
  @IsString()
  doctype: string;

  @ValidateNested()
  @Type(() => UpdateApproversWebookDto)
  leave_approvers: UpdateApproversWebookDto[];

  @ValidateNested()
  @Type(() => UpdateApproversWebookDto)
  expense_approvers: UpdateApproversWebookDto[];
}

export class UpdateApproversWebookDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  approver: string;

  @IsOptional()
  @IsString()
  doctype: string;
}
