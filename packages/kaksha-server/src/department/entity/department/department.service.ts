import { InjectRepository } from '@nestjs/typeorm';
import { Department } from './department.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';

@Injectable()
export class DepartmentService {
  constructor(
    @InjectRepository(Department)
    private readonly departmentRepository: MongoRepository<Department>,
  ) {}

  async find(query?) {
    return await this.departmentRepository.find(query);
  }

  async create(department: Department) {
    const departmentObject = new Department();
    Object.assign(departmentObject, department);
    return await this.departmentRepository.insertOne(departmentObject);
  }

  async findOne(param, options?) {
    return await this.departmentRepository.findOne(param, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.departmentRepository.manager.connection
      .getMetadata(Department)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.departmentRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.departmentRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?) {
    return await this.departmentRepository.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.departmentRepository.updateOne(query, options);
  }
}
