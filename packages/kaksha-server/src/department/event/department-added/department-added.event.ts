import { IEvent } from '@nestjs/cqrs';
import { Department } from '../../entity/department/department.entity';

export class DepartmentAddedEvent implements IEvent {
  constructor(public department: Department, public clientHttpRequest: any) {}
}
