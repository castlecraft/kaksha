import { Test, TestingModule } from '@nestjs/testing';
import { VideoAggregateService } from './video-aggregate.service';
import { VideoService } from '../../entity/video/video.service';
import { TopicService } from '../../../topic/entity/topic/topic.service';
import { CourseService } from '../../../course/entity/course/course.service';

describe('VideoAggregateService', () => {
  let service: VideoAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        VideoAggregateService,
        {
          provide: VideoService,
          useValue: {},
        },
        {
          provide: TopicService,
          useValue: {},
        },
        {
          provide: CourseService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<VideoAggregateService>(VideoAggregateService);
  });
  VideoAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
