import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { v4 as uuidv4 } from 'uuid';
import { VideoDto } from '../../entity/video/video-dto';
import { Video } from '../../entity/video/video.entity';
import { VideoAddedEvent } from '../../event/video-added/video-added.event';
import { VideoService } from '../../entity/video/video.service';
import { VideoRemovedEvent } from '../../event/video-removed/video-removed.event';
import { VideoUpdatedEvent } from '../../event/video-updated/video-updated.event';
import { UpdateVideoDto } from '../../entity/video/update-video-dto';
import { from, throwError, of } from 'rxjs';
import { CourseService } from '../../../course/entity/course/course.service';
import { GetCourseQueryDto } from '../../../constants/listing-dto/get-course-dto';
import { switchMap, mergeMap, toArray } from 'rxjs/operators';
import { COURSE_NOT_FOUND, TOPIC_NOT_FOUND } from '../../../constants/messages';
import { TopicService } from '../../../topic/entity/topic/topic.service';
import { CourseTopicsDto } from '../../../course/entity/course/course-dto';
import { VIDEO } from '../../../constants/app-strings';

@Injectable()
export class VideoAggregateService extends AggregateRoot {
  constructor(
    private readonly videoService: VideoService,
    private readonly courseService: CourseService,
    private readonly topicService: TopicService,
  ) {
    super();
  }

  addVideo(videoPayload: VideoDto, clientHttpRequest) {
    const video = new Video();
    Object.assign(video, videoPayload);
    video.uuid = uuidv4();
    this.apply(new VideoAddedEvent(video, clientHttpRequest));
  }

  async retrieveVideo(uuid: string, req) {
    const provider = await this.videoService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  getVideoList(
    offset,
    limit,
    sort,
    search,
    getvideoPayload: GetCourseQueryDto,
    clientHttpRequest,
  ) {
    return from(
      this.courseService.findOne({
        name: decodeURIComponent(getvideoPayload.course_name),
      }),
    ).pipe(
      switchMap(course => {
        if (!course) {
          return throwError(new NotFoundException(COURSE_NOT_FOUND));
        }
        return this.getTopicList(course.topics).pipe(
          switchMap(topicsList => {
            return this.getVideoName(topicsList).pipe(
              switchMap((videoNameList: Array<string>) => {
                return from(
                  this.videoService.list(
                    offset,
                    limit,
                    search,
                    sort,
                    videoNameList,
                  ),
                ).pipe(
                  switchMap(videoList => {
                    return of(videoList);
                  }),
                );
              }),
            );
          }),
        );
      }),
    );
  }

  getTopicList(topics: CourseTopicsDto[]) {
    return from(topics).pipe(
      mergeMap(topic => {
        return of(topic.topic_name);
      }),
      toArray(),
    );
  }

  getVideoName(topicList: Array<string>) {
    return from(topicList).pipe(
      mergeMap(topicName => {
        return from(this.topicService.findOne({ topic_name: topicName })).pipe(
          switchMap(topic => {
            if (!topic) {
              return throwError(new NotFoundException(TOPIC_NOT_FOUND));
            }
            return from(topic.topic_content).pipe(
              mergeMap(topicContent => {
                if (topicContent.content_type === VIDEO) {
                  return of(topicContent.content);
                }
                return of();
              }),
            );
          }),
        );
      }),
      toArray(),
    );
  }

  async remove(uuid: string) {
    const found = await this.videoService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new VideoRemovedEvent(found));
  }

  async update(updatePayload: UpdateVideoDto) {
    const provider = await this.videoService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new VideoUpdatedEvent(update));
  }
}
