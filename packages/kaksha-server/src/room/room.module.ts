import { Module, HttpModule } from '@nestjs/common';
import { RoomAggregatesManager } from './aggregates';
import { RoomEntitiesModule } from './entity/entity.module';
import { RoomQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { RoomCommandManager } from './command';
import { RoomEventManager } from './event';
import { RoomController } from './controllers/room/room.controller';
import { RoomPoliciesService } from './policies/room-policies/room-policies.service';

@Module({
  imports: [RoomEntitiesModule, CqrsModule, HttpModule],
  controllers: [RoomController],
  providers: [
    ...RoomAggregatesManager,
    ...RoomQueryManager,
    ...RoomEventManager,
    ...RoomCommandManager,
    RoomPoliciesService,
  ],
  exports: [RoomEntitiesModule],
})
export class RoomModule {}
