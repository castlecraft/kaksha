import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { RoomUpdatedEvent } from './room-updated.event';
import { RoomService } from '../../entity/room/room.service';

@EventsHandler(RoomUpdatedEvent)
export class RoomUpdatedCommandHandler
  implements IEventHandler<RoomUpdatedEvent> {
  constructor(private readonly object: RoomService) {}

  async handle(event: RoomUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}
