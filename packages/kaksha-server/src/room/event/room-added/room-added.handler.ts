import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { RoomAddedEvent } from './room-added.event';
import { RoomService } from '../../entity/room/room.service';

@EventsHandler(RoomAddedEvent)
export class RoomAddedCommandHandler implements IEventHandler<RoomAddedEvent> {
  constructor(private readonly roomService: RoomService) {}
  async handle(event: RoomAddedEvent) {
    const { room } = event;
    await this.roomService.create(room);
  }
}
