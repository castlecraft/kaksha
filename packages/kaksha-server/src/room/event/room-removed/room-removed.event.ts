import { IEvent } from '@nestjs/cqrs';
import { Room } from '../../entity/room/room.entity';

export class RoomRemovedEvent implements IEvent {
  constructor(public room: Room) {}
}
