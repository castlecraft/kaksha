import { Test, TestingModule } from '@nestjs/testing';
import { RoomPoliciesService } from './room-policies.service';

describe('RoomPoliciesService', () => {
  let service: RoomPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RoomPoliciesService],
    }).compile();

    service = module.get<RoomPoliciesService>(RoomPoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
