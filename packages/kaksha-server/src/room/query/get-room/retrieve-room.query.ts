import { IQuery } from '@nestjs/cqrs';

export class RetrieveRoomQuery implements IQuery {
  constructor(public readonly uuid: string, public readonly req: any) {}
}
