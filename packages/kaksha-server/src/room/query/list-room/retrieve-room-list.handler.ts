import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveRoomListQuery } from './retrieve-room-list.query';
import { RoomAggregateService } from '../../aggregates/room-aggregate/room-aggregate.service';

@QueryHandler(RetrieveRoomListQuery)
export class RetrieveRoomListQueryHandler
  implements IQueryHandler<RetrieveRoomListQuery> {
  constructor(private readonly manager: RoomAggregateService) {}
  async execute(query: RetrieveRoomListQuery) {
    const { offset, limit, search, sort, clientHttpRequest } = query;
    return await this.manager.getRoomList(
      Number(offset),
      Number(limit),
      search,
      sort,
      clientHttpRequest,
    );
  }
}
