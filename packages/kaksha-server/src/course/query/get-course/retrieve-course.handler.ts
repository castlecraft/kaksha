import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveCourseQuery } from './retrieve-course.query';
import { CourseAggregateService } from '../../aggregates/course-aggregate/course-aggregate.service';

@QueryHandler(RetrieveCourseQuery)
export class RetrieveCourseQueryHandler
  implements IQueryHandler<RetrieveCourseQuery> {
  constructor(private readonly manager: CourseAggregateService) {}

  async execute(query: RetrieveCourseQuery) {
    const { getCoursePayload, req } = query;
    return this.manager.retrieveCourse(getCoursePayload, req);
  }
}
