import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddCourseCommand } from './add-course.command';
import { CourseAggregateService } from '../../aggregates/course-aggregate/course-aggregate.service';

@CommandHandler(AddCourseCommand)
export class AddCourseCommandHandler
  implements ICommandHandler<AddCourseCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: CourseAggregateService,
  ) {}
  async execute(command: AddCourseCommand) {
    const { coursePayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addCourse(coursePayload, clientHttpRequest);
    aggregate.commit();
  }
}
