import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CourseService } from '../../entity/course/course.service';
import { CourseRemovedEvent } from './course-removed.event';

@EventsHandler(CourseRemovedEvent)
export class CourseRemovedCommandHandler
  implements IEventHandler<CourseRemovedEvent> {
  constructor(private readonly courseService: CourseService) {}
  async handle(event: CourseRemovedEvent) {
    const { course } = event;
    await this.courseService.deleteOne({ uuid: course.uuid });
  }
}
